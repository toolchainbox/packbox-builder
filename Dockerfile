FROM python:3.11.4-slim-bookworm

#COPY requirements.txt /

ENV DEBIAN_FRONTEND=noninteractive

RUN apt-get update -y && apt-get upgrade -y

RUN pip install --upgrade pip
RUN pip3 install --no-cache-dir \
        beautifulsoup4 \
        livereload \
        mdx-gh-links \
        mkdocs \
        mkdocs-extra-sass-plugin \
        mkdocs-macros-plugin \
        mkdocs-material \
        mkdocs-material-extensions \
        mkdocs-minify-plugin \
        mkdocs-section-index \
        mkdocs-print-site-plugin  \
        mkdocs-redirects \
        mkdocs-video \
        mkdocs-with-pdf \       
        pymdown-extensions \
        qrcode \
        weasyprint

#        git+https://github.com/jldiaz/mkdocs-plugin-tags.git \
#        git+https://github.com/fralau/mkdocs_macros_plugin.git@v0.5.11




RUN pip3 freeze > /requirements.txt

# Headless Chrome
RUN apt-get install --no-install-recommends -y curl gnupg \
    && curl -sSL https://dl.google.com/linux/linux_signing_key.pub | apt-key add - \
    && echo 'deb [arch=amd64] http://dl.google.com/linux/chrome/deb/ stable main' | tee /etc/apt/sources.list.d/google-chrome.list \
    && apt-get update \
    && apt-get install --no-install-recommends -y google-chrome-stable

RUN echo '#!/bin/sh\n\
exec google-chrome $*\n\
' > /usr/local/bin/chromium-browser \
    && chmod a+x /usr/local/bin/chromium-browser \
    && chromium-browser --version

# Additional font
#COPY fonts /usr/share/fonts/Additional
RUN apt-get install --no-install-recommends -y fontconfig fonts-symbola fonts-noto fonts-freefont-ttf \
                    libpango-1.0-0 libpangoft2-1.0-0 \
    && fc-cache -f \
    && fc-list | sort

RUN apt-get purge --auto-remove -y curl gnupg \
    && rm -rf /var/lib/apt/lists/* /var/cache/apt/* \
    && dpkg -l

# Set working directory and User
RUN useradd -m --uid 1000 mkdocs
USER mkdocs
WORKDIR /docs

# Expose MkDocs development server port
EXPOSE 8000

# Start development server by default
# ENTRYPOINT ["mkdocs"]
# CMD ["serve"]
